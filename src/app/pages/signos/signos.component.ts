import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Signos } from 'src/app/_model/signos';
import { SignosService } from 'src/app/_service/signos.service';

@Component({
  selector: 'app-signos',
  templateUrl: './signos.component.html',
  styleUrls: ['./signos.component.css']
})
export class SignosComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: MatTableDataSource<Signos>;
  displayedColumns: string[] = ['idSignos', 'paciente', 'temperatura', 'pulso', 'ritmo', 'acciones'];
  cantidad: number = 0;

  constructor(
    private signosService: SignosService,
    private snackBar: MatSnackBar,
    public route: ActivatedRoute
  ) { }

  ngOnInit(): void {

    this.signosService.listarPageable(0, 10).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
    });

    this.signosService.getPacienteCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.signosService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000,
        verticalPosition: "top",
        horizontalPosition: "right"
      });
    });

  }

  eliminar(id: number) {
    this.signosService.eliminar(id).subscribe(() => {
      this.signosService.listar().subscribe(data => {
        this.signosService.setPacientecambio(data);
        this.signosService.setMensajeCambio('SE ELIMINO');
      });
    });
  }

  crearTabla(data: Signos[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  filtrar(valor: string) {
    this.dataSource.filterPredicate = function (data: Signos, filter: string) {
      return data.temperatura.toString().toLowerCase().includes(filter)
        ? true
        : data.pulso.toString().toLowerCase().includes(filter)
          ? true
          : data.ritmo.toString().toLowerCase().includes(filter)
            ? true
            : data.paciente.nombres.toString().toLowerCase().includes(filter)
              ? true
              : data.paciente.apellidos.toString().toLowerCase().includes(filter)
                ? true
                : false;
    }
    this.dataSource.filter = valor.trim().toLowerCase();
  }

  mostrarMas(e: any) {
    this.signosService.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
    });
  }

}
