import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Paciente } from 'src/app/_model/paciente';
import { Signos } from 'src/app/_model/signos';
import { PacienteService } from 'src/app/_service/paciente.service';
import { SignosService } from 'src/app/_service/signos.service';

@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {

  pacientes: Paciente[];

  fechaSeleccionada: Date = new Date();

  form: FormGroup;
  id: number;
  idPacienteSeleccionado: number;
  edicion: boolean;

  myControlPaciente: FormControl = new FormControl();

  pacientesFiltrados$: Observable<Paciente[]>;

  constructor(
    private pacienteService: PacienteService,
    private signosService: SignosService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.listarPacientes();

    this.form = new FormGroup({
      'id': new FormControl(0),
      'paciente': this.myControlPaciente,
      'fecha': new FormControl(''),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmo': new FormControl(''),
    });

    this.pacientesFiltrados$ = this.myControlPaciente.valueChanges.pipe(map(val => this.filtrarPacientes(val)));

    this.route.params.subscribe((data: Params) => {
      this.id = data['id'];
      this.edicion = data['id'] != null;
      this.initForm();
    })
  }

  filtrarPacientes(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(el =>
        el.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || el.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || el.dni.includes(val.dni)
      );
    }
    return this.pacientes.filter(el =>
      el.nombres.toLowerCase().includes(val?.toLowerCase()) || el.apellidos.toLowerCase().includes(val?.toLowerCase()) || el.dni.includes(val)
    );
  }

  initForm() {
    if (this.edicion) {
      this.signosService.listarPorId(this.id).subscribe(data => {
        this.form = new FormGroup({
          'paciente': new FormControl(data.paciente),
          'fecha': new FormControl(data.fecha),
          'temperatura': new FormControl(data.temperatura),
          'pulso': new FormControl(data.pulso),
          'ritmo': new FormControl(data.ritmo)
        });
        this.idPacienteSeleccionado = data.paciente.idPaciente;
      });
    }
  }

  mostrarPaciente(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  operar(): void {
    let paciente = new Paciente();
    paciente.idPaciente = this.idPacienteSeleccionado;

    let signo = new Signos();
    signo.idSignos = this.id;
    signo.paciente = this.form.value['paciente'];
    signo.fecha = moment(this.fechaSeleccionada).format('YYYY-MM-DDTHH:mm:ss');
    signo.temperatura = this.form.value['temperatura'];
    signo.pulso = this.form.value['pulso'];
    signo.ritmo = this.form.value['ritmo'];

    if (this.edicion) {
      //MODIFICAR
      this.signosService.modificar(signo).subscribe(() => {
        this.signosService.listar().subscribe(data => {
          this.signosService.setPacientecambio(data);
          this.signosService.setMensajeCambio('SE MODIFICO');
        });
      });
    } else {
      //REGISTRAR
      this.signosService.registrar(signo).subscribe(() => {
        this.signosService.listar().subscribe(data => {
          this.signosService.setPacientecambio(data);
          this.signosService.setMensajeCambio('SE REGISTRO');
        });
      });
    }
    this.router.navigate(['pages', 'signos']);
  }

  listarPacientes() {
    this.pacienteService.listar().subscribe(data => this.pacientes = data);
    // this.pacientes$ = this.pacienteService.listar();
  }

}
